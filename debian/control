Source: golang-github-zmap-zcertificate
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Peymaneh Nejad <p.nejad@posteo.de>
Section: golang
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any,
               golang-github-zmap-zcrypto-dev,
               golang-github-zmap-zlint-dev
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-zmap-zcertificate
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-zmap-zcertificate.git
Homepage: https://github.com/zmap/zcertificate
Rules-Requires-Root: no
XS-Go-Import-Path: github.com/zmap/zcertificate

Package: zcertificate
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Built-Using: ${misc:Built-Using}
Description: Commandline utility for parsing x.509 certificates
 This application parses certificates, runs ZLint on them
 (https://github.com/zmap/zlint) and returns them as JSON to stdout.

Package: golang-github-zmap-zcertificate-dev
Architecture: all
Depends: golang-github-zmap-zcrypto-dev,
         golang-github-zmap-zlint-dev,
         ${misc:Depends}
Description: Go library for parsing x.509 certificates
 This package includes the following functions:
  - ScannerSplitPEM breaks input into chunks that can be handled by pem.decode
    included in the go standard library
  - BreakPEMAsync splits input into decoded PEM objects
  - BreakBase64ByLineAsync reads lines in and sends out each decoded as base64
