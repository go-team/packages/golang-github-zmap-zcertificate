module github.com/zmap/zcertificate

go 1.15

require (
	github.com/sirupsen/logrus v1.7.0
	github.com/zmap/zcrypto v0.0.0-20201211161100-e54a5822fb7e
	github.com/zmap/zlint/v3 v3.0.0
	golang.org/x/crypto v0.0.0-20201208171446-5f87f3452ae9 // indirect
	golang.org/x/net v0.0.0-20201209123823-ac852fbbde11 // indirect
)
